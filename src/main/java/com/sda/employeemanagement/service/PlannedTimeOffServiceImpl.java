package com.sda.employeemanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.employeemanagement.dao.PlannedTimeOffDao;
import com.sda.employeemanagement.model.PlannedTimeOff;

@Service
@Transactional
public class PlannedTimeOffServiceImpl implements PlannedTimeOffService {

	@Autowired
	private PlannedTimeOffDao plannedTimeOffDao;

	public void addPlannedTimeOff(PlannedTimeOff plannedTimeOff) {
		plannedTimeOffDao.addPlannedTimeOff(plannedTimeOff);

	}

	public void removePlannedTimeOff(int plannedTimeOffId) {
		plannedTimeOffDao.removePlannedTimeOff(plannedTimeOffId);

	}

	public void updatePlannedTimeOff(PlannedTimeOff plannedTimeOff) {
		plannedTimeOffDao.updatePlannedTimeOff(plannedTimeOff);

	}

	public PlannedTimeOff getPlannedTimeOffById(int plannedTimeOffId) {
		return plannedTimeOffDao.getPlannedTimeOffById(plannedTimeOffId);
	}

	public List<PlannedTimeOff> getAllTimeOffPlans() {
		return plannedTimeOffDao.getAllTimeOffPlans();
	}

	@Override
	public List<PlannedTimeOff> getPlannedTimeOffRequestsByDepartment(String departmentName) {
		return plannedTimeOffDao.getPlannedTimeOffRequestsByDepartment(departmentName);
	}

	@Override
	public List<PlannedTimeOff> getPlannedTimeOffRequestsByManager(int managerId) {
		return plannedTimeOffDao.getPlannedTimeOffRequestsByManager(managerId);
	}


}
