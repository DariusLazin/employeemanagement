package com.sda.employeemanagement.service;

import java.util.List;

import com.sda.employeemanagement.model.Role;

public interface RoleService {

	public void addRole(Role role);

	public void removeRole(int roleId);

	public void updateRole(Role role);

	public Role getRoleById(int roleId);

	public Role getRoleByName(String roleName);

	public List<Role> getAllRoles();

}
