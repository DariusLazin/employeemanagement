package com.sda.employeemanagement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.sda.employeemanagement.dao.PositionDao;
import com.sda.employeemanagement.model.Position;

@Service
@Transactional
public class PositionServiceImpl implements PositionService {

	@Autowired
	private PositionDao positionDao;

	public void addPosition(Position position) {
		positionDao.addPosition(position);
	}

	public void removePosition(int positionId) {
		positionDao.removePosition(positionId);
	}

	public void updatePosition(Position position) {
		positionDao.updatePosition(position);

	}

	public Position getPositionById(int positionId) {
		return positionDao.getPositionById(positionId);
	}

	public Position getPositionByName(String positionName) {
		return positionDao.getPositionByName(positionName);
	}

	public List<Position> getAllPositions() {
		return positionDao.getAllPositions();
	}

}
