package com.sda.employeemanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.employeemanagement.dao.DepartmentDao;
import com.sda.employeemanagement.model.Department;

@Service
@Transactional
public class DepartmentServiceImpl implements DepartmentService {

	@Autowired
	private DepartmentDao departmentDao;

	public void addDepartment(Department department) {
		departmentDao.addDepartment(department);

	}

	public void removeDepartment(int departmentId) {
		departmentDao.removeDepartment(departmentId);

	}

	public void updateDepartment(Department department) {
		departmentDao.updateDepartment(department);

	}

	public Department getDepartmentById(int departmentId) {
		return departmentDao.getDepartmentById(departmentId);
	}

	public Department getDepartmentByName(String departmentName) {
		return departmentDao.getDepartmentByName(departmentName);
	}

	public List<String> getDepartmentsName() {
		return departmentDao.getDepartmentsName();
	}

	public List<Department> getAllDepartments() {
		return departmentDao.getAllDepartments();
	}

}
