package com.sda.employeemanagement.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;



@Service
@Transactional
public class AuthenticationService implements UserDetailsService{

	@Autowired
	private EmployeeService employeeService;

	// converteste obiectul de tip user in obiect de tip UserDetails care il
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		com.sda.employeemanagement.model.Employee domainUser = employeeService.getEmployeeByUsername(username);

		return new User(domainUser.getUsername(), domainUser.getPassword(), getAuthorities(domainUser));
	}

	// converteste rolurile din baza de date in obiecte de tip GrantedAuthority
	public Collection<? extends GrantedAuthority> getAuthorities(com.sda.employeemanagement.model.Employee employee) {
		List<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
		
		authorities.add(new SimpleGrantedAuthority(employee.getRole().getName()));
		
		
		return authorities ;
	
	}
}
