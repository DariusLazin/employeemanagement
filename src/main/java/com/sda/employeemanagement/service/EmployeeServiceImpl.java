package com.sda.employeemanagement.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.sda.employeemanagement.dao.EmployeeDao;
import com.sda.employeemanagement.model.Employee;

@Service
@Transactional
public class EmployeeServiceImpl implements EmployeeService {

	@Autowired
	private EmployeeDao employeeDao;
	
	public void addEmployee(Employee employee) {
		employeeDao.addEmployee(employee);

	}

	public void removeEmployee(int employeeId) {
		employeeDao.removeEmployee(employeeId);

	}

	public void updateEmployee(Employee employee) {
		employeeDao.updateEmployee(employee);

	}

	public Employee getEmployeeById(int employeeId) {
		return employeeDao.getEmployeeById(employeeId);
	}

	public List<Employee> getEmployeeByFirstName(String employeeFirstName) {
		return employeeDao.getEmployeeByFirstName(employeeFirstName);
	}

	public List<Employee> getAllEmployees() {
		return employeeDao.getAllEmployees();
	}

	@Override
	public Employee getEmployeeByUsername(String username) {
		return employeeDao.getEmployeeByUsername(username);
	}

	@Override
	public List<Employee> getEmployeesFromSameDepartment(String departmentName) {
		return employeeDao.getEmployeesFromSameDepartment(departmentName);
	}

	@Override
	public List<Employee> getAllManagers(String managerRole) {
		return employeeDao.getAllManagers(managerRole);
	}

}
