package com.sda.employeemanagement.service;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.sda.employeemanagement.dao.RoleDao;
import com.sda.employeemanagement.model.Role;

@Service
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao roleDao;

	@Override
	public void addRole(Role role) {
		roleDao.addRole(role);

	}

	@Override
	public void removeRole(int roleId) {
		roleDao.removeRole(roleId);

	}

	@Override
	public void updateRole(Role role) {
		roleDao.updateRole(role);

	}

	@Override
	public Role getRoleById(int roleId) {
		return roleDao.getRoleById(roleId);
	}

	@Override
	public Role getRoleByName(String roleName) {
		return roleDao.getRoleByName(roleName);
	}

	@Override
	public List<Role> getAllRoles() {
		return roleDao.getAllRoles();
	}

}
