package com.sda.employeemanagement.service;

import java.util.List;

import com.sda.employeemanagement.model.Position;

public interface PositionService {

	public void addPosition(Position position);

	public void removePosition(int positionId);

	public void updatePosition(Position position);

	public Position getPositionById(int positionId);

	public Position getPositionByName(String positionName);

	public List<Position> getAllPositions();
}
