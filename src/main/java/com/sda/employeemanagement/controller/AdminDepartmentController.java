package com.sda.employeemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.employeemanagement.model.Department;
import com.sda.employeemanagement.service.DepartmentService;

@Controller
@RequestMapping("/department")
public class AdminDepartmentController {

	@Autowired
	private DepartmentService departmentService;

	@RequestMapping(value = "listDepartments", method = RequestMethod.GET)
	public String listDepartments(ModelMap model) {
		model.addAttribute("DepartmentsList", departmentService.getAllDepartments());

		model.addAttribute("department", new Department());
		return "adminDepartmentsList";
	}

	@RequestMapping(value = "deleteDepartmentById", method = RequestMethod.POST)
	public String deleteDepartment(ModelMap model, @RequestParam("id") int id) {

		departmentService.removeDepartment(id);
		model.addAttribute("department", new Department());
		model.addAttribute("DepartmentsList", departmentService.getAllDepartments());

		return "adminDepartmentsList";
	}

	@RequestMapping(value = "addDepartment", method = RequestMethod.POST)
	public String addDepartment(ModelMap model, @ModelAttribute("department") Department department) {

		departmentService.addDepartment(department);

		model.addAttribute("DepartmentsList", departmentService.getAllDepartments());
		return "adminDepartmentsList";

	}

	@RequestMapping(value = "updateDepartmentById/{id}", method = RequestMethod.GET)
	public String updateDepartment(@ModelAttribute("department") Department department, ModelMap model,
			@PathVariable("id") int id) {
		Department departmentToBeUpdated = departmentService.getDepartmentById(id);
		model.addAttribute("department", departmentToBeUpdated);

		return "adminDepartmentUpdate";

	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public String saveUpdatedDepartmentInfo(@ModelAttribute("department") Department department, ModelMap model) {
		departmentService.updateDepartment(department);
		model.addAttribute("DepartmentsList", departmentService.getAllDepartments());

		return "adminDepartmentsList";

	}
}
