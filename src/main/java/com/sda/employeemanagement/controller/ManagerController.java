package com.sda.employeemanagement.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.employeemanagement.model.Employee;
import com.sda.employeemanagement.model.PlannedTimeOff;
import com.sda.employeemanagement.service.EmployeeService;
import com.sda.employeemanagement.service.PlannedTimeOffService;

@Controller
@RequestMapping("/manager")
public class ManagerController {

	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private PlannedTimeOffService plannedTimeOffService;
	
	@RequestMapping(value = "listEmployees", method = RequestMethod.GET)
	public String listEmployeesFromSameDepartment(ModelMap model) {
		
		
		Authentication auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		String usernameLoggedIn = auth.getName();
		
		Employee managerLoggedIn = employeeService.getEmployeeByUsername(usernameLoggedIn);
		String managerLoggedInDepartment = managerLoggedIn.getDepartment().getName();
		int managerLoggedInId = managerLoggedIn.getId();
		
		List<Employee> employeesFromSameDepartment = employeeService.getEmployeesFromSameDepartment(managerLoggedInDepartment);
		List<PlannedTimeOff> timeOffRequestsForManager = plannedTimeOffService.getPlannedTimeOffRequestsByManager(managerLoggedInId);
		Set<PlannedTimeOff> timeOffRequestsForManagerSet = new LinkedHashSet<>(timeOffRequestsForManager);
		
		model.addAttribute("employeesFromSameDepartmentList", employeesFromSameDepartment);
		model.addAttribute("managerName", managerLoggedIn.getFirstName() + " " + managerLoggedIn.getLastName());
		model.addAttribute("managerDepartment", managerLoggedIn.getDepartment().getName());
		model.addAttribute("managerPosition", managerLoggedIn.getPosition().getName());
		model.addAttribute("timeOffRequestsForManager", timeOffRequestsForManagerSet);
		
		return "manager";
	}
	
	@RequestMapping(value = "deletePlannedTimeOffById", method = RequestMethod.POST)
	public String deletePlannedTimeOff(ModelMap model, @RequestParam("id") int id) {
		Authentication auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		String usernameLoggedIn = auth.getName();
		
		Employee employeeLoggedIn = employeeService.getEmployeeByUsername(usernameLoggedIn);
		String employeeLoggedInDepartment = employeeLoggedIn.getDepartment().getName();
		
		plannedTimeOffService.removePlannedTimeOff(id);
		model.addAttribute("timeOffRequestsForManager", plannedTimeOffService.getPlannedTimeOffRequestsByDepartment(employeeLoggedInDepartment));

		return "redirect:/manager/listEmployees";
	}
	
	@RequestMapping(value = "approveTimeOff", method = RequestMethod.POST)
	public String approveTimeOff(ModelMap model, @RequestParam("id") int id) {
		Authentication auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		String usernameLoggedIn = auth.getName();
		
		Employee employeeLoggedIn = employeeService.getEmployeeByUsername(usernameLoggedIn);
		String employeeLoggedInDepartment = employeeLoggedIn.getDepartment().getName();
		
		PlannedTimeOff plannedTimeOff = plannedTimeOffService.getPlannedTimeOffById(id);
		plannedTimeOff.setApproved(true);
		plannedTimeOffService.updatePlannedTimeOff(plannedTimeOff);
		
		model.addAttribute("timeOffRequestsForManager", plannedTimeOffService.getPlannedTimeOffRequestsByDepartment(employeeLoggedInDepartment));

		return "redirect:/manager/listEmployees";
	}
	
	@RequestMapping(value = "denyTimeOff", method = RequestMethod.POST)
	public String denyTimeOff(ModelMap model, @RequestParam("id") int id) {
		Authentication auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		String usernameLoggedIn = auth.getName();
		
		Employee employeeLoggedIn = employeeService.getEmployeeByUsername(usernameLoggedIn);
		String employeeLoggedInDepartment = employeeLoggedIn.getDepartment().getName();
		
		PlannedTimeOff plannedTimeOff = plannedTimeOffService.getPlannedTimeOffById(id);
		plannedTimeOff.setApproved(false);
		plannedTimeOffService.updatePlannedTimeOff(plannedTimeOff);
		
		model.addAttribute("timeOffRequestsForManager", plannedTimeOffService.getPlannedTimeOffRequestsByDepartment(employeeLoggedInDepartment));

		return "redirect:/manager/listEmployees";
	}
	
	@InitBinder
	private void dateBinder(WebDataBinder binder) {
		// The date format to parse or output your dates
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		// Create a new CustomDateEditor
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		// Register it as custom editor for the Date type
		binder.registerCustomEditor(Date.class, editor);
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }
}
