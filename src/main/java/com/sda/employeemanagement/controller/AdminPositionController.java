package com.sda.employeemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.employeemanagement.model.Position;
import com.sda.employeemanagement.service.PositionService;

@Controller
@RequestMapping("/position")
public class AdminPositionController {

	@Autowired
	private PositionService positionService;

	@RequestMapping(value = "listPositions", method = RequestMethod.GET)
	public String listPositions(ModelMap model) {
		model.addAttribute("PositionsList", positionService.getAllPositions());

		model.addAttribute("position", new Position());
		return "adminPositionsList";
	}

	@RequestMapping(value = "deletePositionById", method = RequestMethod.POST)
	public String deletePosition(ModelMap model, @RequestParam("id") int id) {

		model.addAttribute("position", new Position());
		positionService.removePosition(id);
		model.addAttribute("PositionsList", positionService.getAllPositions());

		return "adminPositionsList";
	}

	@RequestMapping(value = "addPosition", method = RequestMethod.POST)
	public String addPosition(ModelMap model, @ModelAttribute("position") Position position) {

		positionService.addPosition(position);

		model.addAttribute("PositionsList", positionService.getAllPositions());
		return "adminPositionsList";

	}

	@RequestMapping(value = "updatePositionById/{id}", method = RequestMethod.GET)
	public String updatePosition(@ModelAttribute("position") Position position, ModelMap model,
			@PathVariable("id") int id) {

		Position positionToBeUpdated = positionService.getPositionById(id);
		model.addAttribute("position", positionToBeUpdated);

		return "adminPositionUpdate";

	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public String saveUpdatedPositionInfo(@ModelAttribute("position") Position position, ModelMap model) {
		positionService.updatePosition(position);
		model.addAttribute("PositionsList", positionService.getAllPositions());

		return "adminPositionsList";

	}

}
