package com.sda.employeemanagement.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.employeemanagement.model.Employee;
import com.sda.employeemanagement.model.Role;
import com.sda.employeemanagement.service.EmployeeService;


@Controller
public class LoginController {
	
	@Autowired
	private EmployeeService employeeService;

	@RequestMapping("/login")
	public String login(ModelMap model, @RequestParam(name="error", required=false) String error, @RequestParam(name="logout", required=false) String logout) {
		if (error != null) {
            model.addAttribute("error", "Invalid username and password!");
        }
		
		if (logout != null) {
			model.addAttribute("logout", "You have been logged out successfully");
		}
		return "login";
	}

	@RequestMapping("/successful")
	public String printUser(ModelMap model) {

		Authentication auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		// get logged in username
		String usernameLoggedIn = auth.getName();

		// get the employee name according to the username
		Employee employeeLoggedIn = employeeService.getEmployeeByUsername(usernameLoggedIn);

		// get the role of the logged user
		Role employeeLoggedInRole = employeeLoggedIn.getRole();

		// just for checking
		System.out.println("Name is: " + employeeLoggedIn.getFirstName() + " " + employeeLoggedIn.getLastName());
		System.out.println("Role: " + employeeLoggedInRole.getName());

		if (employeeLoggedInRole.getName().equals("ROLE_ADMIN")) {
			model.addAttribute("userDetails", employeeLoggedIn.getFirstName());
			return "adminHome";
		} else if (employeeLoggedInRole.getName().equals("ROLE_MANAGER")) {
			model.addAttribute("userDetails", employeeLoggedIn.getFirstName());
			return "redirect:/manager/listEmployees";
		} else if (employeeLoggedInRole.getName().equals("ROLE_EMPLOYEE")) {
			model.addAttribute("userDetails", employeeLoggedIn.getFirstName());

			return "redirect:/employee/listEmployees";
		}

		return null;

	}
}
