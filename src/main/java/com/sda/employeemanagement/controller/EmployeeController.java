package com.sda.employeemanagement.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.sda.employeemanagement.model.Employee;
import com.sda.employeemanagement.model.PlannedTimeOff;
import com.sda.employeemanagement.service.EmployeeService;
import com.sda.employeemanagement.service.PlannedTimeOffService;

@Controller
@RequestMapping("/employee")
public class EmployeeController {
	
	@Autowired
	private EmployeeService employeeService;
	
	@Autowired
	private PlannedTimeOffService plannedTimeOffService;
	
	@RequestMapping(value = "listEmployees", method = RequestMethod.GET)
	public String listEmployeesFromSameDepartment(ModelMap model) {
		
		Authentication auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		String usernameLoggedIn = auth.getName();
		
		Employee employeeLoggedIn = employeeService.getEmployeeByUsername(usernameLoggedIn);
		String employeeLoggedInDepartment = employeeLoggedIn.getDepartment().getName();
		System.out.println("Empl controller: " + employeeLoggedIn.getFirstName() + " " + employeeLoggedIn.getLastName() + " " + employeeLoggedIn.getDepartment().getName());
		
		List<Employee> employeesFromSameDepartment = employeeService.getEmployeesFromSameDepartment(employeeLoggedInDepartment);
		
		model.addAttribute("employeesFromSameDepartmentList", employeesFromSameDepartment);
		model.addAttribute("employeeName", employeeLoggedIn.getFirstName() + " " + employeeLoggedIn.getLastName());
		model.addAttribute("employeeDepartment", employeeLoggedIn.getDepartment().getName());
		model.addAttribute("employeePosition", employeeLoggedIn.getPosition().getName());
		model.addAttribute("employeeManager", employeeLoggedIn.getManager().getFirstName() + " " + employeeLoggedIn.getManager().getLastName());
		model.addAttribute("employeePlannedTimeOffHistory", employeeLoggedIn.getPlannedTimeOff());

		return "employee";
	}
	
	@RequestMapping(value = "requestTimeOffArea", method = RequestMethod.GET)
	public String requestTimeOffArea(ModelMap model) {
		
		Authentication auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		String usernameLoggedIn = auth.getName();
		
		Employee employeeLoggedIn = employeeService.getEmployeeByUsername(usernameLoggedIn);
		
		PlannedTimeOff plannedTimeOff = new PlannedTimeOff();
		plannedTimeOff.setEmployee(employeeLoggedIn);
		model.addAttribute("plannedTimeOff", plannedTimeOff);
		
		return "employeePlannedTimeOffSubmit";
	}
	
	@RequestMapping(value = "requestTimeOff", method = RequestMethod.POST)
	public String requestTimeOff(ModelMap model, @ModelAttribute("plannedTimeOff") PlannedTimeOff plannedTimeOff) {
		
		Authentication auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		String usernameLoggedIn = auth.getName();
		
		Employee employeeLoggedIn = employeeService.getEmployeeByUsername(usernameLoggedIn);
		
		plannedTimeOffService.addPlannedTimeOff(plannedTimeOff);
		model.addAttribute("employeePlannedTimeOffHistory", employeeLoggedIn.getPlannedTimeOff());
		
		return "redirect:/employee/listEmployees";
	}
	
	@RequestMapping(value = "deletePlannedTimeOffById", method = RequestMethod.POST)
	public String deletePlannedTimeOff(ModelMap model, @RequestParam("id") int id) {
		Authentication auth = (Authentication) SecurityContextHolder.getContext().getAuthentication();
		String usernameLoggedIn = auth.getName();
		
		Employee employeeLoggedIn = employeeService.getEmployeeByUsername(usernameLoggedIn);
		
		plannedTimeOffService.removePlannedTimeOff(id);
		model.addAttribute("employeePlannedTimeOffHistory", employeeLoggedIn.getPlannedTimeOff());

		return "redirect:/employee/listEmployees";
	}
	
	@InitBinder
	private void dateBinder(WebDataBinder binder) {
		// The date format to parse or output your dates
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		// Create a new CustomDateEditor
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		// Register it as custom editor for the Date type
		binder.registerCustomEditor(Date.class, editor);
	}
	
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }
	
}
