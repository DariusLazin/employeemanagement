package com.sda.employeemanagement.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.thymeleaf.util.StringUtils;

import com.sda.employeemanagement.model.Department;
import com.sda.employeemanagement.model.Employee;
import com.sda.employeemanagement.model.Position;
import com.sda.employeemanagement.model.Role;
import com.sda.employeemanagement.service.DepartmentService;
import com.sda.employeemanagement.service.EmployeeService;
import com.sda.employeemanagement.service.PositionService;
import com.sda.employeemanagement.service.RoleService;

@Controller
@RequestMapping("/employeeAdmin")
public class AdminEmployeeController {

	@Autowired
	private DepartmentService departmentService;

	@Autowired
	private EmployeeService employeeService;

	@Autowired
	private PositionService positionService;
	
	@Autowired
	private RoleService roleService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@RequestMapping(value = "/adminHome")
	public String getAdminHomePage() {

		return "adminHome";
	}

	@RequestMapping(value = "listEmployees", method = RequestMethod.GET)
	public String listEmployees(ModelMap model) {
		model.addAttribute("employeesList", employeeService.getAllEmployees());
		
		return "adminEmployeeList";
	}

	@RequestMapping(value = "/search")
	public String SearchEmployeeByName(@RequestParam("employeeFirstName") String employeeFirstName, ModelMap model) {
		if (StringUtils.isEmptyOrWhitespace(employeeFirstName)) {
			model.addAttribute("employeesList", employeeService.getAllEmployees());
		} else {
			List<Employee> searchedEmployee = (List<Employee>) employeeService.getEmployeeByFirstName(employeeFirstName);
			model.addAttribute("searchedEmployee", searchedEmployee);

		}

		return "adminEmployeeList";
	}

	@RequestMapping(value = "deleteEmployeeById", method = RequestMethod.POST)
	public String deleteEmployee(ModelMap model, @RequestParam("id") int id) {
		employeeService.removeEmployee(id);
		model.addAttribute("employeesList", employeeService.getAllEmployees());

		return "adminEmployeeList";
	}

	@RequestMapping(value = "addEmployeeArea", method = RequestMethod.GET)
	public String accessAddEmployeeArea(ModelMap model) {
	
		Employee employee = new Employee();
		employee.setPosition(new Position());
		employee.setDepartment(new Department());
		employee.setRole(new Role());
		
		model.addAttribute("employee", employee);
		model.addAttribute("departmentsList", departmentService.getAllDepartments());
		model.addAttribute("positionsList", positionService.getAllPositions());
		model.addAttribute("rolesList", roleService.getAllRoles());
		model.addAttribute("managersList", employeeService.getAllManagers("ROLE_MANAGER"));
		
		return "adminEmployeeAdd";
	}

	@RequestMapping(value = "addEmployee", method = RequestMethod.POST)
	public String addEmployee(@Valid @ModelAttribute("employee") Employee employee, BindingResult result, ModelMap model) {
		
		if (result.hasErrors()) {
			model.addAttribute("departmentsList", departmentService.getAllDepartments());
			model.addAttribute("positionsList", positionService.getAllPositions());
			model.addAttribute("rolesList", roleService.getAllRoles());
			model.addAttribute("managersList", employeeService.getAllManagers("ROLE_MANAGER"));
			return "adminEmployeeAdd";
		}
		
		//encrypt password
		employee.setPassword(passwordEncoder.encode(employee.getPassword()));
		
		employeeService.addEmployee(employee);
		return "redirect:/employeeAdmin/listEmployees";

	}

	@RequestMapping(value = "updateEmployeeById/{id}", method = RequestMethod.GET)
	public String updateEmployee(@ModelAttribute("employee") Employee employee, ModelMap model,
			@PathVariable("id") int id) {

		Employee employeeToBeUpdated = employeeService.getEmployeeById(id);
		model.addAttribute("employee", employeeToBeUpdated);
		model.addAttribute("departmentsList", departmentService.getAllDepartments());
		model.addAttribute("positionsList", positionService.getAllPositions());
		model.addAttribute("rolesList", roleService.getAllRoles());
		model.addAttribute("managersList", employeeService.getAllManagers("ROLE_MANAGER"));
		
		return "adminEmployeeUpdate";

	}

	@RequestMapping(value = "save", method = RequestMethod.POST)
	public String saveUpdatedEmployeeInfo(@ModelAttribute("employee") Employee employee, ModelMap model) {
		
		//encrypt password before updating
		employee.setPassword(passwordEncoder.encode(employee.getPassword()));
		
		employeeService.updateEmployee(employee);
		model.addAttribute("employeesList", employeeService.getAllEmployees());

		return "adminEmployeeList";

	}

	@InitBinder
	private void dateBinder(WebDataBinder binder) {
		// The date format to parse or output your dates
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		// Create a new CustomDateEditor
		CustomDateEditor editor = new CustomDateEditor(dateFormat, true);
		// Register it as custom editor for the Date type
		binder.registerCustomEditor(Date.class, editor);
	}

	@RequestMapping(value = "/logout", method = RequestMethod.GET)
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (auth != null) {
            new SecurityContextLogoutHandler().logout(request, response, auth);
        }
        return "redirect:/login?logout";
    }

}
