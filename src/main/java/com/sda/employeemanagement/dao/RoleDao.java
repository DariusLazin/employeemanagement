package com.sda.employeemanagement.dao;

import java.util.List;

import com.sda.employeemanagement.model.Role;

public interface RoleDao {

	public void addRole(Role role);

	public void removeRole(int roleId);

	public void updateRole(Role role);

	public Role getRoleById(int roleId);

	public Role getRoleByName(String roleName);

	public List<Role> getAllRoles();
	
}
