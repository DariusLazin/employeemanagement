package com.sda.employeemanagement.dao;

import java.util.List;

import com.sda.employeemanagement.model.Position;

public interface PositionDao {

	public void addPosition(Position position);

	public void removePosition(int positionId);

	public void updatePosition(Position position);

	public Position getPositionById(int positionId);

	public Position getPositionByName(String positionName);

	public List<Position> getAllPositions();

}
