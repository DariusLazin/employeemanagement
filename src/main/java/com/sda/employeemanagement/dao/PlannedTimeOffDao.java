package com.sda.employeemanagement.dao;

import java.util.List;

import com.sda.employeemanagement.model.PlannedTimeOff;

public interface PlannedTimeOffDao {

	public void addPlannedTimeOff(PlannedTimeOff plannedTimeOff);

	public void removePlannedTimeOff(int plannedTimeOffId);

	public void updatePlannedTimeOff(PlannedTimeOff plannedTimeOff);

	public PlannedTimeOff getPlannedTimeOffById(int plannedTimeOffId);
	
	public List<PlannedTimeOff> getPlannedTimeOffRequestsByDepartment(String departmentName);
	
	public List<PlannedTimeOff> getPlannedTimeOffRequestsByManager(int managerId);

	public List<PlannedTimeOff> getAllTimeOffPlans();
	
}
