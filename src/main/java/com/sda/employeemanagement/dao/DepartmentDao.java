package com.sda.employeemanagement.dao;

import java.util.List;

import com.sda.employeemanagement.model.Department;

public interface DepartmentDao {

	public void addDepartment(Department department);

	public void removeDepartment(int departmentId);

	public void updateDepartment(Department department);

	public Department getDepartmentById(int departmentId);

	public Department getDepartmentByName(String departmentName);
	
	public List<String> getDepartmentsName();
	
	public List<Department> getAllDepartments();
}
