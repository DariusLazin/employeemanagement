package com.sda.employeemanagement.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.sda.employeemanagement.model.Department;

@Repository
public class DepartmentDaoImpl implements DepartmentDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void addDepartment(Department department) {
		sessionFactory.getCurrentSession().save(department);

	}

	public void removeDepartment(int departmentId) {
		//sessionFactory.getCurrentSession().delete(getDepartmentById(departmentId));
		
		String hql = "delete Department where id = :param"; 
		Query q = sessionFactory.getCurrentSession().createQuery(hql).setParameter("param", departmentId);
		q.executeUpdate();
	}

	public void updateDepartment(Department department) {
		sessionFactory.getCurrentSession().update(department);

	}

	public Department getDepartmentById(int departmentId) {
		return (Department) sessionFactory.getCurrentSession().get(Department.class, departmentId);
	}

	public Department getDepartmentByName(String departmentName) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Department where name = :param")
				.setParameter("param", departmentName);

		return (Department) query.uniqueResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> getDepartmentsName() {
		Query query = sessionFactory.getCurrentSession().createQuery("select name from Department");
		
		return (List<String>) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Department> getAllDepartments() {
		return sessionFactory.getCurrentSession().createQuery("from Department").list();
	}

	

}
