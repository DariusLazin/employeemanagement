package com.sda.employeemanagement.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sda.employeemanagement.model.Role;

@Repository
public class RoleDaoImpl implements RoleDao {

	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public void addRole(Role role) {
		sessionFactory.getCurrentSession().save(role);

	}

	@Override
	public void removeRole(int roleId) {
		String hql = "delete Role where id = :param";
		Query q = sessionFactory.getCurrentSession().createQuery(hql).setParameter("param", roleId);
		q.executeUpdate();

	}

	@Override
	public void updateRole(Role role) {
		sessionFactory.getCurrentSession().update(role);

	}

	@Override
	public Role getRoleById(int roleId) {
		return (Role) sessionFactory.getCurrentSession().get(Role.class, roleId);
	}

	@Override
	public Role getRoleByName(String roleName) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Role where name = :param")
				.setParameter("param", roleName);

		return (Role) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Role> getAllRoles() {
		return sessionFactory.getCurrentSession().createQuery("from Role").list();
	}

}
