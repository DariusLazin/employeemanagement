package com.sda.employeemanagement.dao;

import java.util.List;

import com.sda.employeemanagement.model.Employee;

public interface EmployeeDao {

	public void addEmployee(Employee employee);

	public void removeEmployee(int employeeId);

	public void updateEmployee(Employee employee);

	public Employee getEmployeeById(int employeeId);

	public List<Employee> getEmployeeByFirstName(String employeeFirstName);
	
	public Employee getEmployeeByUsername(String username);
	
	public List<Employee> getEmployeesFromSameDepartment(String departmentName);
	
	public List<Employee> getAllManagers(String managerRole);

	public List<Employee> getAllEmployees();
}
