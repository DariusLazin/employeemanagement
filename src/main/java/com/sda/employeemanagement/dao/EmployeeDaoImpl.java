package com.sda.employeemanagement.dao;



import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import com.sda.employeemanagement.model.Employee;

@Repository
public class EmployeeDaoImpl implements EmployeeDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void addEmployee(Employee employee) {
		sessionFactory.getCurrentSession().save(employee);

	}

	public void removeEmployee(int employeeId) {
		//sessionFactory.getCurrentSession().delete(getEmployeeById(employeeId));
		
		String hql = "delete Employee where id = :param"; 
		Query q = sessionFactory.getCurrentSession().createQuery(hql).setParameter("param", employeeId);
		q.executeUpdate();
	}

	public void updateEmployee(Employee employee) {
		sessionFactory.getCurrentSession().update(employee);

	}

	public Employee getEmployeeById(int employeeId) {
		return (Employee) sessionFactory.getCurrentSession().get(Employee.class, employeeId);
	}

	@SuppressWarnings("unchecked")
	public List<Employee> getEmployeeByFirstName(String employeeFirstName) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Employee where firstName = :param")
				.setParameter("param", employeeFirstName);

		return query.list();
	}

	@SuppressWarnings("unchecked")
	public List<Employee> getAllEmployees() {
		return sessionFactory.getCurrentSession().createQuery("from Employee").list();
	}

	@Override
	public Employee getEmployeeByUsername(String username) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Employee where username = :param")
				.setParameter("param", username);

		return (Employee) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getEmployeesFromSameDepartment(String departmentName) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Employee where department.name = :param").setParameter("param", departmentName);

		return query.list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Employee> getAllManagers(String managerRole) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Employee where role.name = :param")
				.setParameter("param", managerRole);

		return query.list();
	}

}
