package com.sda.employeemanagement.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sda.employeemanagement.model.PlannedTimeOff;

@Repository
public class PlannedTimeOffDaoImpl implements PlannedTimeOffDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void addPlannedTimeOff(PlannedTimeOff plannedTimeOff) {
		sessionFactory.getCurrentSession().save(plannedTimeOff);

	}

	public void removePlannedTimeOff(int plannedTimeOffId) {
		// sessionFactory.getCurrentSession().delete(getPlannedTimeOffById(plannedTimeOffId));

		String hql = "delete PlannedTimeOff where id = :param";
		Query q = sessionFactory.getCurrentSession().createQuery(hql).setParameter("param", plannedTimeOffId);
		q.executeUpdate();
	}

	public void updatePlannedTimeOff(PlannedTimeOff plannedTimeOff) {
		sessionFactory.getCurrentSession().update(plannedTimeOff);

	}

	public PlannedTimeOff getPlannedTimeOffById(int plannedTimeOffId) {
		return (PlannedTimeOff) sessionFactory.getCurrentSession().get(PlannedTimeOff.class, plannedTimeOffId);
	}

	@SuppressWarnings("unchecked")
	public List<PlannedTimeOff> getAllTimeOffPlans() {
		return sessionFactory.getCurrentSession().createQuery("from PlannedTimeOff").list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PlannedTimeOff> getPlannedTimeOffRequestsByDepartment(String departmentName) {
		
		return (List<PlannedTimeOff>) sessionFactory.getCurrentSession().createCriteria(PlannedTimeOff.class).createAlias("employee", "employee").createAlias("employee.department", "department").add(Restrictions.eq("department.name", departmentName)).list();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<PlannedTimeOff> getPlannedTimeOffRequestsByManager(int managerId) {
		
		return (List<PlannedTimeOff>) sessionFactory.getCurrentSession().createCriteria(PlannedTimeOff.class).createAlias("employee", "employee").add(Restrictions.eq("employee.manager.id", managerId)).list();
	}

}
