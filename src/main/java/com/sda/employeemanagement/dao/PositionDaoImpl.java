package com.sda.employeemanagement.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.sda.employeemanagement.model.Position;

@Repository
public class PositionDaoImpl implements PositionDao {

	@Autowired
	private SessionFactory sessionFactory;

	public void addPosition(Position position) {
		sessionFactory.getCurrentSession().save(position);

	}

	public void removePosition(int positionId) {
		//sessionFactory.getCurrentSession().delete(getPositionById(positionId));

		String hql = "delete Position where id = :param"; 
		Query q = sessionFactory.getCurrentSession().createQuery(hql).setParameter("param", positionId);
		q.executeUpdate();
	}

	public void updatePosition(Position position) {
		sessionFactory.getCurrentSession().update(position);

	}

	public Position getPositionById(int positionId) {
		return (Position) sessionFactory.getCurrentSession().get(Position.class, positionId);
	}

	public Position getPositionByName(String positionName) {
		Query query = sessionFactory.getCurrentSession().createQuery("from Position where name = :param")
				.setParameter("param", positionName);

		return (Position) query.uniqueResult();
	}

	@SuppressWarnings("unchecked")
	public List<Position> getAllPositions() {
		return sessionFactory.getCurrentSession().createQuery("from Position").list();
	}

}
