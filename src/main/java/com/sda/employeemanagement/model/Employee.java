package com.sda.employeemanagement.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

@Entity
@Table(name = "employee")
public class Employee {
	@Id
	@Column(name = "EMPLOYEE_ID_PK")
	private int id;
	@Column(name = "FIRST_NAME")
	@NotEmpty
	private String firstName;
	@Column(name = "LAST_NAME")
	@NotEmpty
	private String lastName;
	@Column(name = "EMAIL")
	@Email
	@NotEmpty
	private String email;
	@Column(name = "PHONE_NUMBER")
	@NotEmpty
	private String phoneNumber;
	@Column(name = "ADDRESS")
	@NotEmpty
	private String address;
	@Column(name = "DATE_OF_BIRTH")
	@NotNull
	private Date dateOfBirth;
	@Column(name = "SALARY")
	@NotNull
	private Double salary;
	@Column(name = "USERNAME", unique = true)
	@NotEmpty
	private String username;
	@Column(name = "PASSWORD")
	@NotEmpty
	private String password;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "POSITION_ID_FK")
	@NotNull
	private Position position;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "DEPARTMENT_ID_FK")
	@NotNull
	private Department department;

	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "ROLE_ID_FK")
	@NotNull
	private Role role;

	@OneToMany(fetch = FetchType.EAGER, mappedBy = "employee")
	private List<PlannedTimeOff> plannedTimeOff;
	
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name = "MANAGER_ID")
	private Employee manager;

	public Employee() {

	}

	public Employee(int id, String firstName, String lastName, String email, String phoneNumber, String address,
			Date dateOfBirth, Double salary, String username, String password, Position position, Department department,
			Role role, List<PlannedTimeOff> plannedTimeOff, Employee manager) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.phoneNumber = phoneNumber;
		this.address = address;
		this.dateOfBirth = dateOfBirth;
		this.salary = salary;
		this.username = username;
		this.password = password;
		this.position = position;
		this.department = department;
		this.role = role;
		this.plannedTimeOff = plannedTimeOff;
		this.manager = manager;
	}



	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Date getDateOfBirth() {
		return dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public Double getSalary() {
		return salary;
	}

	public void setSalary(Double salary) {
		this.salary = salary;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public Position getPosition() {
		return position;
	}

	public void setPosition(Position position) {
		this.position = position;
	}

	public Department getDepartment() {
		return department;
	}

	public void setDepartment(Department department) {
		this.department = department;
	}

	public List<PlannedTimeOff> getPlannedTimeOff() {
		return plannedTimeOff;
	}

	public void setPlannedTimeOff(List<PlannedTimeOff> plannedTimeOff) {
		this.plannedTimeOff = plannedTimeOff;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Employee getManager() {
		return manager;
	}

	public void setManager(Employee manager) {
		this.manager = manager;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Employee other = (Employee) obj;
		if (id != other.id)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", email=" + email
				+ ", phoneNumber=" + phoneNumber + ", address=" + address + ", dateOfBirth=" + dateOfBirth + ", salary="
				+ salary + ", username=" + username + ", password=" + password + ", position=" + position
				+ ", department=" + department + ", role=" + role + ", plannedTimeOff=" + plannedTimeOff + "]";
	}
	
	

}
