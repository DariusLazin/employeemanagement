<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style><%@include file="/WEB-INF/resources/css/loginPage.css"%></style>

</head>
<body background="/EmployeeManagement/WebContent/WEB-INF/resources/images/wallpaper.jpg">

	<div class="loginBox">
		<form name='loginForm'
			  action="<c:url value='j_spring_security_check' />" method='POST'>
	
				<h1>Login</h1><hr class="underline" ><br>
				
				
				<div class="textBox">
					<div class="userIcon">
						<i class="fa fa-user" aria-hidden="true"></i>
					</div>
					<input class ="userAndPass" type='text' name='username' placeholder="Username">
				</div>
			
				<div class="textBox">
					<div class="passIcon">
						<i class="fa fa-lock" aria-hidden="true"></i>
					</div>
					<input class ="userAndPass" type='password' name='password' placeholder="Password"/>
				</div><br><br>
			
				<input class ="loginButton" name="submit" type="submit" value="Sign In" />
			
		 
		  
		  		<h4 class="invalidUsernameAndPasswordMessage">${error}</h4>
		  		<h4 class="logoutMessage">${logout}</h4>
		 
		</form>
	 </div>
</body>
</html>