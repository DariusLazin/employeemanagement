<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Insert title here</title>
	<style><%@include file="/WEB-INF/resources/css/styling.css"%></style>
</head>
<body>

	<div class="header">
		<h1 class="title">Employee Management<br>Welcome back, <security:authentication property="principal.username" /></h1>
		<a href="<c:url value="/employee/logout" />">Logout</a>
	</div>
	<br><br>
	
	<div class="employeeDetailsContainer">
	<h2>EMPLOYEE DETAILS</h2><hr>
	<table class="emplDetailsTable">
		<tr>
			<td><b>FULL NAME:</b></td>
			<td class="emplDetailsColor"><b>${employeeName}</b></td>
		</tr>
		<tr>
			<td><b>DEPARTMENT:</b></td>
			<td class="emplDetailsColor"><b>${employeeDepartment}</b></td>
		</tr>
		<tr>
			<td><b>POSITION:</b></td>
			<td class="emplDetailsColor"><b>${employeePosition}</b></td>
		</tr>
		<tr>
			<td><b>MANAGER:</b></td>
			<td class="emplDetailsColor"><b>${employeeManager}</b></td>
		</tr>

	</table>
	</div><br>
	<br>
	<br>
	<div class="employeeSameDepContainer">
	<h2>EMPLOYEES FROM YOUR DEPARTMENT</h2>
	<hr>
	<table>
		<thead class="tableHead">
			<tr>
				<td><b>ID</b></td>
				<td><b>FIRST NAME</b></td>
				<td><b>LAST NAME</b></td>
				<td><b>EMAIL</b></td>
				<td><b>PHONE</b></td>
				<td><b>BIRTHDAY</b></td>
				<td><b>DEPARTMENT</b></td>
				<td><b>POSITION</b></td>
			</tr>
		</thead>
		<tbody class="tableBody">
			<c:forEach items="${employeesFromSameDepartmentList}" var="iterator" varStatus="row">
				<tr>
					<td><c:out value="${row.count}"></c:out></td>
					<td><c:out value="${iterator.getFirstName()}"></c:out></td>
					<td><c:out value="${iterator.getLastName()}"></c:out></td>
					<td><c:out value="${iterator.getEmail()}"></c:out></td>
					<td><c:out value="${iterator.getPhoneNumber()}"></c:out></td>
					<fmt:formatDate value="${iterator.getDateOfBirth()}"
						pattern="yyyy-MM-dd" var="dateOfBirth" />
					<td><c:out value="${dateOfBirth}"></c:out></td>
					<td><c:out value="${iterator.getDepartment().getName()}"></c:out></td>
					<td><c:out value="${iterator.getPosition().getName()}"></c:out></td>
				</tr>
			</c:forEach>
		</tbody>

	</table>
	</div><br><br>
	
	<div class="plannedTimeOff">
	<h2>PLANNED TIME OFF HISTORY</h2><hr>
	<table>
		<thead class="tableHead" id="plannedTimeOffId">
			<tr>
				<td><b>ID</b></td>
				<td><b>START DATE</b></td>
				<td><b>END DATE</b></td>
				<td><b>APPROVAL</b></td>

			</tr>
		</thead>
		<tbody class="tableBody">
			<c:forEach items="${employeePlannedTimeOffHistory}" var="iterator" varStatus="row">
				<tr>
					<td><c:out value="${row.count}"></c:out></td>
					<fmt:formatDate value="${iterator.getStartDate()}"
						pattern="yyyy-MM-dd" var="startDate" />
					<td><c:out value="${startDate}"></c:out></td>
					<fmt:formatDate value="${iterator.getEndDate()}"
						pattern="yyyy-MM-dd" var="endDate" />
					<td><c:out value="${endDate}"></c:out></td>
					
					<c:if test="${iterator.isApproved() eq true}">
						<td><c:out value="Approved"></c:out></td>
					</c:if>
					
					<c:if test="${iterator.isApproved() eq false}">
						<td><c:out value="Denied"></c:out></td>
					</c:if>
					
					<c:if test="${empty iterator.isApproved()}">
						<td><c:out value="Pending"></c:out></td>
					</c:if>
					
					<td>
						<form method="POST"
							action="${pageContext.request.contextPath}/employee/deletePlannedTimeOffById?id=${iterator.getId()}">
							<input class="deleteButton" type="submit" value="Delete">
						</form>
					</td>
				</tr>
			</c:forEach>
		</tbody>
	
	</table>
	
	</div><br><br>
	
	<div class="reqTimeOffButtonArea">
		<a class="requestTimeOffButton" href="/EmployeeManagement/employee/requestTimeOffArea">Request Days Off</a><br>
	</div>
	<br>
	<br>
	
	<div class="footer"></div>

</body>
</html>