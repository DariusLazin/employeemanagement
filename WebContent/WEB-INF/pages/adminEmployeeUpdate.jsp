<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Update Employee</title>
	<style><%@include file="/WEB-INF/resources/css/styling.css"%></style>
</head>
<body>
	
	<div class="header">
		<h1 class="title">Employee Management<br>Logged in as - <security:authentication property="principal.username" /></h1>
		<a href="<c:url value="/employeeAdmin/logout" />">Logout</a>
	</div>
	<br>
	
	<div class="backButton">
	<a href="/EmployeeManagement/employeeAdmin/listEmployees">Back</a>
	</div>
	<br>


	<div class="adminEmployeeUpdateContainer">
	<h1>EMPLOYEE UPDATE</h1><hr>
	<spring:url value="/employeeAdmin/save" var="saveURL"></spring:url>
	<form:form method="POST" action="${saveURL}" modelAttribute="employee">

		<table class="adminEmployeeUpdateTable">
			<tr>
				<td><form:label path="id">ID:</form:label></td>
				<td><form:input class="textArea" path="id" readonly="true" /></td>
			</tr>

			<tr>
				<td><form:label path="firstName">FIRST NAME:</form:label></td>
				<td><form:input class="textArea" path="firstName" /></td>
			</tr>

			<tr>
				<td><form:label path="lastName">LAST NAME:</form:label></td>
				<td><form:input class="textArea" path="lastName" /></td>
			</tr>

			<tr>
				<td><form:label path="email">EMAIL:</form:label></td>
				<td><form:input class="textArea" path="email" /></td>
			</tr>

			<tr>
				<td><form:label path="phoneNumber">PHONE:</form:label></td>
				<td><form:input class="textArea" path="phoneNumber" /></td>
			</tr>

			<tr>
				<td><form:label path="address">ADDRESS:</form:label></td>
				<td><form:input class="textArea" path="address" /></td>
			</tr>

			<tr>
				<td><form:label path="dateOfBirth">BIRTHDAY:</form:label></td>
				<fmt:formatDate value="${dateOfBirth}" pattern="yyyy-MM-dd"
					var="dateOfBirth" />
				<td><form:input class="textArea" path="dateOfBirth" type="date" /></td>
			</tr>

			<tr>
				<td><form:label path="salary">SALARY:</form:label></td>
				<td><form:input class="textArea" path="salary" /></td>
			</tr>

			<tr>
				<td><form:label path="username">USERNAME:</form:label></td>
				<td><form:input class="textArea" path="username" /></td>
			</tr>

			 <tr>
				<td><form:label path="password">PASSWORD:</form:label></td>
				<td><form:input class="textArea" path="password" /></td>
			</tr>

			<tr>
				<td>DEPARTMENT:</td>
				<td><form:select path="department.id">
						<option selected value="-1" disabled>Select Department</option>
						<c:forEach var="dep" items="${departmentsList}">
							<option value="${dep.id}" ${dep.getName() eq employee.getDepartment().getName() ? 'selected' : ''}>
							<c:out value="${dep.name}" /></option>
						</c:forEach>
					</form:select></td>
			</tr>

			<tr>
				<td>POSITION:</td>
				<td><form:select path="position.id">
						<option selected value="-1" disabled>Select Position</option>
						<c:forEach var="pos" items="${positionsList}">
							<option value="${pos.id}" ${pos.getName() eq employee.getPosition().getName() ? 'selected' : ''}>
							<c:out value="${pos.name}" /></option>
						</c:forEach>

					</form:select></td>
			</tr>
			
			<tr>
				<td>ROLE:</td>
				<td><form:select path="role.id">
						<option selected value="-1" disabled>Select Role</option>
						<c:forEach var="role" items="${rolesList}">
							<option value="${role.id}" ${role.getName() eq employee.getRole().getName() ? 'selected' : ''}>
							<c:out value="${role.name}" /></option>
						</c:forEach>

					</form:select></td>
			</tr>
			
			<tr>
				<td>MANAGER:</td>
				<td><form:select path="manager.id">
						<option selected value="-1" disabled>Select Manager</option>
						<c:forEach var="manager" items="${managersList}">
							<option value="${manager.id}" ${manager.getFirstName() eq employee.getManager().getFirstName() ? 'selected' : ''}>
							<c:out value="${manager.firstName}" /></option>
						</c:forEach>

					</form:select></td>
			</tr>
			
			<br>
			<tr>
				<td><input class="admExistingEmplUpdateButton" type="submit" value="Update" /><td>
			</tr>

		</table>
	</form:form>
	</div><br><br>
	
	<div class="footer"></div>
</body>
</html>