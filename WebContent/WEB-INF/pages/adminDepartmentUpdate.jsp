<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style><%@include file="/WEB-INF/resources/css/styling.css"%></style>
</head>
<body>
	
	<div class="header">
		<h1 class="title">Employee Management<br>Logged in as - <security:authentication property="principal.username" /></h1>
		<a href="<c:url value="/employeeAdmin/logout" />">Logout</a>
	</div>
	<br>
	
	<div class="backButton">
		<a href="/EmployeeManagement/department/listDepartments">Back</a><br><br>
	</div>
	
	<div class="adminDepartmentUpdateAreaContainer">
	<spring:url value="/department/save" var="saveURL"></spring:url>
	<form:form method="POST" action="${saveURL}"
		modelAttribute="department">
	<h1>UPDATE DEPARTMENT</h1><hr>
		<table class="updateDepartmentForm">

			<tr>
				<td><form:label path="id">ID:</form:label></td>
				<td><form:input class="textArea" path="id" readonly="true" /></td>
			</tr>

			<tr>
				<td><form:label path="name">DEPARTMENT NAME:</form:label></td>
				<td><form:input class="textArea" path="name" /></td>
			</tr>

			<tr>
				<td><input class="updateDepartmentButton" type="submit" value="Update" /><td>
			</tr>

		</table>

	</form:form>
	</div>
	
	
</body>
</html>