<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>

<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style><%@include file="/WEB-INF/resources/css/styling.css"%></style>
</head>
<body>

	<div class="header">
		<h1 class="title">Employee Management<br>Logged in as - <security:authentication property="principal.username" /></h1>
		<a href="<c:url value="/employeeAdmin/logout" />">Logout</a>
	</div>
	<br>
	
	<div class="backButton">
	<a href="/EmployeeManagement/employeeAdmin/adminHome">Back</a><br><br>
	</div>
	
	<div class="adminPositionsListContainer">
	<h1>POSITIONS</h1><hr>
	<table>
		<thead class="tableHead">
			<tr>
				<td><b>ID</b></td>
				<td><b>POSITION</b></td>
			</tr>
		</thead>
		<tbody class="tableBody">

			<c:forEach items="${PositionsList}" var="iterator" varStatus="row">
				<tr>
					<td><c:out value="${row.count}"></c:out></td>
					<td><c:out value="${iterator.name}"></c:out></td>

					<td>
						<form method="POST"
							action="${pageContext.request.contextPath}/position/deletePositionById?id=${iterator.getId()}">
							<input class="deleteButton" type="submit" value="Delete">
						</form>
					</td>

					<td>
						<form method="GET"
							action="${pageContext.request.contextPath}/position/updatePositionById/${iterator.getId()}">
							<input class="editButton" type="submit" value="Edit">
						</form>
					</td>
				</tr>
			</c:forEach>

		</tbody>
	</table>
	</div><br><br><br>
	
	<div class="adminPositionAdd">
	<h1>Add New Position</h1><hr>
	<spring:url value="/position/addPosition" var="addURL"></spring:url>
	<form:form method="POST" action="${addURL}" modelAttribute="position">

		<table class="addPositionForm">
			<tr>
				<td>POSITION NAME</td>
				<td><form:input class="textArea" path="name" placeholder="Position name" /></td>
			</tr>

			<tr>
				<td><input class="addPositionButton" type="submit" name="add" value="Add Position"></td>
			</tr>

		</table>
	</form:form>
	</div><br><br>
	
	<div class="footer"></div>
</body>
</html>