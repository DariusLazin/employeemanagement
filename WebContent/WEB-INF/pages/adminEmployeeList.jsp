<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Employee Management</title>
<style><%@include file="/WEB-INF/resources/css/styling.css"%></style>
</head>
<body>

	<div class="header">
		<h1 class="title">Employee Management<br>Logged in as - <security:authentication property="principal.username" /></h1>
		<a href="<c:url value="/employeeAdmin/logout" />">Logout</a>
	</div>
	<br>
	
	<div class="backButton">
		<a href="/EmployeeManagement/employeeAdmin/adminHome">Back</a>
	</div><br>
	
	<div class="adminEmployeeListContainer">
		<h1>EMPLOYEE DATABASE</h1><hr>
		<div class="searchArea">
		<form method="get"
			action="${pageContext.request.contextPath}/employeeAdmin/search">
			<input class="searchBar" type="text" name="employeeFirstName"
				placeholder="search by First Name">
			<button class="searchIcon" type="submit">&#8981;</button>
		</form>

		<form method="get"
			action="${pageContext.request.contextPath}/employeeAdmin/listEmployees">
			<button class="refreshIcon" type="submit">&#10227;</button>
		</form>
		</div><br><br>
	
	<table>
		<thead class="tableHead">
			<tr>
				<td><b>ID</b></td>
				<td><b>FIRST NAME</b></td>
				<td><b>LAST NAME</b></td>
				<td><b>EMAIL</b></td>
				<td><b>PHONE</b></td>
				<td><b>ADDRESS</b></td>
				<td><b>BIRTHDAY</b></td>
				<td><b>SALARY</b></td>
				<td><b>USERNAME</b></td>
			<!-- 	<td><b>PASSWORD</b></td> -->
				<td><b>DEPARTMENT</b></td>
				<td><b>POSITION</b></td>
				<td><b>MANAGER</b></td>
				<td><b>ROLE</b></td>
				
			</tr>
		</thead>

		<tbody class="tableBody">

			<c:if test="${searchedEmployee.size() != 0}">
				<c:forEach items="${searchedEmployee}" var="searchedEmployee" varStatus="row" >
					<tr>
						<td>${row.count}</td>
						<td>${searchedEmployee.getFirstName()}</td>
						<td>${searchedEmployee.getLastName()}</td>
						<td>${searchedEmployee.getEmail()}</td>
						<td>${searchedEmployee.getPhoneNumber()}</td>
						<td>${searchedEmployee.getAddress()}</td>
						<fmt:formatDate value="${searchedEmployee.getDateOfBirth()}"
							pattern="yyyy-MM-dd" var="dateOfBirth" />
						<td>${dateOfBirth}</td>
						<td>${searchedEmployee.getSalary()}</td>
						<td>${searchedEmployee.getUsername()}</td>
						<%-- <td>${searchedEmployee.getPassword()}</td> --%>
						<td>${searchedEmployee.getDepartment().getName()}</td>
						<td>${searchedEmployee.getPosition().getName()}</td>
						<td>${searchedEmployee.getManager().getFirstName()}</td>
						<td>${searchedEmployee.getRole().getName()}</td>
		
						<td>
							<form method="POST"
								action="${pageContext.request.contextPath}/employeeAdmin/deleteEmployeeById?id=${searchedEmployee.getId()}">

								<input class="deleteButton" type="submit" value="Delete">
							</form>

						</td>
						<td>
							<form method="GET"
								action="${pageContext.request.contextPath}/employeeAdmin/updateEmployeeById/${searchedEmployee.getId()}">
								<input class="editButton" type="submit" value="Edit">
							</form>
						</td>
					</tr>
				</c:forEach>
			</c:if>

			<c:forEach items="${employeesList}" var="iterator" varStatus="row">
				<tr>
					<td><c:out value="${row.count}"></c:out></td>
					<td><c:out value="${iterator.firstName}"></c:out></td>
					<td><c:out value="${iterator.lastName}"></c:out></td>
					<td><c:out value="${iterator.email}"></c:out></td>
					<td><c:out value="${iterator.phoneNumber}"></c:out></td>
					<td><c:out value="${iterator.address}"></c:out></td>
					<fmt:formatDate value="${iterator.dateOfBirth}"
						pattern="yyyy-MM-dd" var="dateOfBirth" />
					<td><c:out value="${dateOfBirth}"></c:out></td>
					<td><c:out value="${iterator.salary}"></c:out></td>
					<td><c:out value="${iterator.username}"></c:out></td>
					<%-- <td><c:out value="${iterator.password}"></c:out></td> --%>
					<td><c:out value="${iterator.department.name}"></c:out></td>
					<td><c:out value="${iterator.position.name}"></c:out></td>
					<td><c:out value="${iterator.manager.firstName}"></c:out></td>
					<td><c:out value="${iterator.role.name}"></c:out></td>
			
					<td>
						<form method="POST"
							action="${pageContext.request.contextPath}/employeeAdmin/deleteEmployeeById?id=${iterator.getId()}">
							<input class="deleteButton" type="submit" value="Delete">
						</form>
					</td>

					<td>
						<form method="GET"
							action="${pageContext.request.contextPath}/employeeAdmin/updateEmployeeById/${iterator.getId()}">
							<input class="editButton" type="submit" value="Edit">
						</form>
					</td>
					
				</tr>
			</c:forEach>
			


		</tbody>
	</table>
	</div>
	<br>

	<div class="footer"></div>

</body>
</html>