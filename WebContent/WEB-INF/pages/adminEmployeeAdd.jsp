<%@page import="java.text.SimpleDateFormat"%>
<%@page import="java.util.Date"%>
<%@page import="java.text.Format"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style><%@include file="/WEB-INF/resources/css/styling.css"%></style>
</head>
<body>

	<div class="header">
		<h1 class="title">Employee Management<br>Logged in as - <security:authentication property="principal.username" /></h1>
		<a href="<c:url value="/employeeAdmin/logout" />">Logout</a>
	</div>
	<br>
	
	<div class="backButton">
	<a href="/EmployeeManagement/employeeAdmin/adminHome">Back</a>
	</div>
	<br>

	<div class="adminEmployeeAddContainer">
	<h1>ADD NEW EMPLOYEE</h1><hr>
	<spring:url value="/employeeAdmin/addEmployee" var="addURL"></spring:url>
	<form:form method="POST" action="${addURL}" modelAttribute="employee">

		<table>
			<tr>
				<td>FIRST NAME:</td>
				<td><form:input class="textArea" path="firstName" placeholder="First Name" /></td>
				<td><form:errors class="fieldError" path="firstName" /></td>
			</tr>

			<tr>
				<td>LAST NAME:</td>
				<td><form:input class="textArea" path="lastName" placeholder="Last Name"/></td>
				<td><form:errors class="fieldError" path="lastName" /></td>
			</tr>

			<tr>
				<td>EMAIL:</td>
				<td><form:input class="textArea" path="email" placeholder="Email"/></td>
				<td><form:errors class="fieldError" path="email" /></td>
			</tr>

			<tr>
				<td>PHONE:</td>
				<td><form:input class="textArea" path="phoneNumber" type="number" placeholder="Phone Number" /></td>
				<td><form:errors class="fieldError" path="phoneNumber" /></td>
			</tr>

			<tr>
				<td>ADDRESS:</td>
				<td><form:input class="textArea" path="address" placeholder="Address"/></td>
				<td><form:errors class="fieldError" path="address" /></td>
			</tr>

			<tr>
				<td>BIRTHDAY:</td>
				<td><form:input class="textArea" path="dateOfBirth" type="date"/></td>
				<td><form:errors class="fieldError" path="dateOfBirth" /></td>
			</tr>

			<tr>
				<td>SALARY:</td>
				<td><form:input class="textArea" path="salary" type="number" placeholder="Salary"/></td>
				<td><form:errors class="fieldError" path="salary" /></td>
			</tr>

			<tr>
				<td>USERNAME:</td>
				<td><form:input class="textArea" path="username" placeholder="Username" /></td>
				<td><form:errors class="fieldError" path="username" /></td>
			</tr>

			<tr>
				<td>PASSWORD:</td>
				<td><form:input class="textArea" path="password" placeholder="Password" /></td>
				<td><form:errors class="fieldError" path="password" /></td>
			</tr>

			<tr>
				<td>DEPARTMENT:</td>
				<td><form:select class="textArea" path="department.id">
						<option selected value="-1" disabled>Select
							Department</option>
						<c:forEach var="dep" items="${departmentsList}">
							<option value="${dep.id}"><c:out
									value="${dep.name}" /></option>
						</c:forEach>
				</form:select></td>
				<td><form:errors class="fieldError" path="department.id" /></td>
			</tr>

			<tr>
				<td>POSITION:</td>
				<td><form:select class="textArea" path="position.id">
						<option selected value="-1" disabled>Select
							Position</option>
						<c:forEach var="pos" items="${positionsList}">
							<option value="${pos.id}"><c:out
									value="${pos.name}" /></option>
						</c:forEach>

				</form:select></td>
				<td><form:errors class="fieldError" path="position.id" /></td>
			</tr>
			
			<tr>
				<td>ROLE:</td>
				<td><form:select class="textArea" path="role.id">
						<option selected value="-1" disabled>Select
							Role</option>
						<c:forEach var="role" items="${rolesList}">
							<option value="${role.id}"><c:out
									value="${role.name}" /></option>
						</c:forEach>

				</form:select></td>
				<td><form:errors class="fieldError" path="role.id" /></td>
			</tr>
			
			<tr>
				<td>MANAGER:</td>
				<td><form:select class="textArea" path="manager.id">
						<option selected value="-1" disabled>Select
							Manager</option>
						<c:forEach var="manager" items="${managersList}">
							<option value="${manager.id}"><c:out
									value="${manager.firstName}" /></option>
						</c:forEach>

				</form:select></td>
			</tr>

			<tr>
				<td><input class="admEmplAddButton" type="submit" name="add" value="Add Employee"></td>
			</tr>

		</table>
	</form:form>
</div><br><br>

<div class="footer"></div>
</body>
</html>