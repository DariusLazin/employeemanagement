<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<!DOCTYPE html>
<html>
<head>
	<meta charset="ISO-8859-1">
	<title>Admin Home</title>
	
	<style><%@include file="/WEB-INF/resources/css/styling.css"%></style>
</head>
<body>
	
	<div class="header">
		<h1 class="title">Employee Management<br>Welcome back, <security:authentication property="principal.username" /></h1>
		<a href="<c:url value="/employeeAdmin/logout" />">Logout</a>
	</div>
	
	<div class="line"></div>
	<br>
	
	<div class="bodyContent"><br>
		
			<h2>EMPLOYEES</h2>
			<a href="/EmployeeManagement/employeeAdmin/listEmployees">Search/Delete/Edit Employee</a><br><br>
			<a href="/EmployeeManagement/employeeAdmin/addEmployeeArea">Add New Employee</a><br><br>

			<h2>DEPARTMENTS</h2>
			<a href="/EmployeeManagement/department/listDepartments">Add/Delete/Edit Departments</a><br><br>
	
			<h2>POSITIONS</h2>
			<a href="/EmployeeManagement/position/listPositions">Add/Delete/Edit Positions</a>
	
	</div><br>
	
	<div class="footer"></div>
</body>
</html>