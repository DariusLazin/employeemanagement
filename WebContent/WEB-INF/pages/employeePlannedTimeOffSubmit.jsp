<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags" %>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>
<style><%@include file="/WEB-INF/resources/css/styling.css"%></style>
</head>
<body>

	<div class="header">
		<h1 class="title">Employee Management<br>Logged in as - <security:authentication property="principal.username" /></h1>
		<a href="<c:url value="/employee/logout" />">Logout</a>
	</div>
	<br><br>

	<div class="backButton">
		<a href="/EmployeeManagement/employee/listEmployees">Back</a><br>
	</div><br>
	
	<div class="requestTimeOffContainer">
	<h1>REQUEST TIME OFF</h1><hr>
	<spring:url value="/employee/requestTimeOff" var="submitURL"></spring:url>
	<form:form method="POST" action="${submitURL}" modelAttribute="plannedTimeOff">
		<table class="requestTimeOffTable">
		
			<tr>
				<td><b>EMPLOYEE ID:</b></td>
				<td><form:input class="textArea" path="employee.id" readonly="true"/></td>
			</tr>
			
			<tr>
				<td><b>START DATE:</b></td>
				<td><form:input class="textArea" path="startDate" type="date" /></td>
			</tr>

			<tr>
				<td><b>END DATE:</b></td>
				<td><form:input class="textArea" path="endDate" type="date" /></td>
			</tr>
			
			<tr>
				<td><input class="requestTimeOffSubmitButton" type="submit" name="add" value="Submit"></td>
			</tr>
			
		</table>
	</form:form>
	</div>
</body>
</html>